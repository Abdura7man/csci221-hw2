#include <iostream>
using namespace std;
#include <iomanip>
#include <stdio.h>

// struct TreeNode;
struct node{
	int data;
	struct node* left;
	struct node* right;
};
struct Node{
    int data;
    Node *left, *right;
    Node(int x)
    {
        data = x;
        left = right = NULL;
    }
};

struct node* newNode(int data){
	struct node* anode = new struct node;
	anode->data = data;
	anode->left = NULL;
	anode->right = NULL;

	return anode;
}

void insert(struct node** rootRef, int data){
	if(*rootRef == NULL){ //there is no right/left children
		*rootRef = newNode(data);
	}else{
		if(data < (*rootRef)->data ){  //if data is less than root
			insert(&(*rootRef)->left, data);
		}else{
			insert(&(*rootRef)->right, data); //if data is greater than root
		}
	}
}

void printInOrder(struct node* root){

	if(root == NULL) return;
	printInOrder(root->left);
	cout << root->data << " ";
	printInOrder(root->right);
}

void printPostOrder(struct node* root){

	if(root == NULL) return;
	printInOrder(root->left);
	printInOrder(root->right);
	cout << root->data << " ";
}


void printPreOrder(struct node* root){

	if(root == NULL) return;
	cout << root->data << " ";
	printInOrder(root->left);
	printInOrder(root->right);
}

int size(struct node* root){
	if(root == NULL) return 0;
	return (size(root->left)+size(root->right)+1);

}

int height(struct node* root){
	if(root == NULL) return 0;
	return (1+max(height(root->left), height(root->right)));

}

//Find the smallest value in BST (With a loop)
int findMinValueLoop(struct node* root){
	if(root == NULL){
		return 0;
	}else{
		struct node* current = root;
		while(current->left != NULL){
			current = current->left;
		}
		return current->data;
	}
}

//Find the smallest value in BST (Recursively)
int findMinValueRec(struct node* root){

	if(root == NULL){
		return 0;
	}else{
		if(root->left == NULL){
			return root->data;
		}
		return findMinValueRec(root->left);
	}

}

//Find the maximum value in BST (Recursively)
int findMaxValueRec(struct node* root){

	if(root == NULL){
		return 0;
	}else{
		if(root->right == NULL){
			return root->data;
		}
		return findMaxValueRec(root->right);
	}

}

struct node **getSuccessor(struct node** root){
	struct node **temp = root;
	while( (*temp) -> right != NULL){
		temp = &(*temp)->right;
	}
	return temp;

}

void deleteNode(struct node** root, int target){

	if(*root != NULL){
		if(target == (*root)->data){
			if((*root)->right == NULL){
				*root = (*root)->left;
			}else if((*root)->left == NULL){
				*root = (*root)->right;
			}else{
				struct node **successor = getSuccessor(&(*root)->left);
				(*root)->data = (*successor)->data;
				deleteNode(successor,(*successor)->data);
			}
		}else if(target < (*root)->data){
			deleteNode(&(*root)->left, target);
		}else{ //if (target > (*root)->data)
			deleteNode(&(*root)->right, target);
		}

	}

}
// check if its BST 
int isBST(struct node* node) 
{ 
if (node == NULL) 
	return true; 
	
/* false if left is > than node */
if (node->left != NULL && node->left->data > node->data) 
	return false; 
	
/* false if right is < than node */
if (node->right != NULL && node->right->data < node->data) 
	return false; 

/* false if, recursively, the left or right is not a BST */
if (!isBST(node->left) || !isBST(node->right)) 
	return false; 
	
/* passing all that, it's a BST */
return true; 
}
//Second requierment:
struct node* insert(struct node* node, int data) 
{
    /* 1. If the tree is empty, return a new, single node */
    if (node == NULL)    
        return(newNode(data));

    /* 2. Otherwise, recurse down the tree */
    else
    {
        if (data <= node->data)
            node->left  = insert(node->left, data);
        else
            node->right = insert(node->right, data);

        /* return the (unchanged) node pointer */
        return node;
    }
}
// Third requiremnet:
// void int ReturnKthSmallestElement1(int k)
//     {
// 		k = value;
//         Node node = Root;

//         int count = k;

//         int sizeOfLeftSubtree = 0;

//         while(node != null)
//         {

//             sizeOfLeftSubtree = node.SizeOfLeftSubtree();

//             if (sizeOfLeftSubtree + 1 == count)
//                 return node.Value;
//             else if (sizeOfLeftSubtree < count)
//             {
//                 node = node.Right;
//                 count -= sizeOfLeftSubtree+1;
//             }
//             else
//             {
//                 node = node.Left;
//             }
//         }

//         return -1;
//     }

// void kthInorder(TreeNode* root, int k, int& count, int& result){
// 	if(root->Left)
// 		kthInorder(root->Left, k, count, result);
// 		count++;
// 		if(count == k){
// 			result = count;
// 			return;
// 		}
// 		if(root->right)
// 			kthInorder(root->right, k, count, result);
// }

// };
// int kthSmallest(TreeNode* root, int k) {
// 	int count = 0;
// 	int result = 0;
// 	kthInorder(root, k, count, result);
// 	return result;
// }


// node* insert(node* root, int x)
// {
//     if (root == NULL)
//         return new Node(x);
//     if (x < root->data)
//         root->left = insert(root->left, x);
//     else if (x > root->data)
//         root->right = insert(root->right, x);
//     return root;
// }
 
// // Here count denotes the number of nodes processed so far
// node* kthSmallest(node* root, int& k)
// {
//     // base case
//     if (root == NULL)
//         return NULL;
 
//     // search in left subtree
//     node* left = kthSmallest(root->left, k);
 
//     // if k'th smallest is found in left subtree, return it
//     if (left != NULL)
//         return left;
 
//     // if current element is k'th smallest, return it
//     k--;
//     if (k == 0)
//         return root;
 


 // Third

node* getnewNode(int val)
{
	node* nn = new node();
	nn->left  = NULL;
	nn->right = NULL;
	nn->data = val;
	return nn;
}
node* k_Smallest(node* rt,int &k)
{
	if(rt==NULL)
	{
		return NULL;
	}
	node* left = k_Smallest(rt->left,k);
	if(left!=NULL)
		return left;
	k--;
	if(k==0)
		return rt;
	return k_Smallest(rt->right,k);
		
}

void getMinimumDifferenceCore(struct node* root,int &lastValue,int &res) {

	if (!root) return;

	if (root->left) getMinimumDifferenceCore(root->left, lastValue, res);
	if (abs(root->data - lastValue) < res) res = (abs(root->data - lastValue));

	lastValue = root->data;
	if (root->right) getMinimumDifferenceCore(root->right, lastValue, res);
}
int getMinimumDifference(struct node* root) {
	int res = INT8_MAX;
	int lastValue = -10000;
	getMinimumDifferenceCore(root, lastValue,res);
	return res;
}

 
// fourth 
// void inorder(struct node* root, int[] A)   
//     {
//         if(root == NULL)
//             return;
//         inorder(root->left, A)
//         A.add(root->data)            
//         inorder(root->right, A)
//     }
// int getMinimumAbsDiff(struct node* root) {
//     int [] arr
//     int min_so_far = INT_MAX 
//     inorder(root, A)
//     for(int i=0 to A.length -1 ; i=i+1)
//     {
//     if ((A[i+1] - A[i]) < min_so_far)
//         min_so_far = A[i+1] - A[i]
//     }
//     return min_so_far
// }


int main() {

	node* root = NULL;
	int k;

	insert(&root, 30);
	insert(&root, 50);
	insert(&root, 70);
	insert(&root, 10);
	insert(&root, 90);
	insert(&root, 40);
	insert(&root, 80);
	int value;
	cout << "Type the value for the address: ";
	cin >> value;
	cout<<"Enter K to get the Kth Smallest Element: "<<endl;
	cin>>k;
	// int a;
	// a = k + 1;

	cout << "In-order: ";
	printInOrder(root);
	cout << endl;
	cout << "Pre-order: " ;
	printPreOrder(root);
	cout << endl;
	cout << "Post-order: ";
	printPostOrder(root);
	cout << endl;

	cout << "The size of the BST: " << size(root) << endl;
	cout << "The height of the BST: " << height(root) << endl;
	cout << "The smallest value of BST: " << findMinValueRec(root) << endl;
	deleteNode(&root, 90);
	cout << "Delete 90 " << endl;

	cout << "Maximum: " << findMaxValueRec(root) << endl;
	cout << "Minimum: " << findMinValueRec(root) << endl;
	cout << "check if it's BST: " << isBST(root) << endl;
	cout << "search an address for a giving value: " << insert(root, value) << endl;
	node* temp;
	temp = k_Smallest(root, k);
	if(temp!=NULL)
		cout<<"Kth Smallest element is "<<temp->data<<endl;
	else
		cout<<"Invalid Input"<<endl;
    }


    cout << "the diffrence is: " << getMinimumDifferenceCore(root) << endl;

	return 0;
}